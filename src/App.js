import React from 'react';
import MainPage from './modules/mainPage/MainPage.jsx';
 
const App = () => {
  return (
    <MainPage/>
  )
}

export default React.memo(App);