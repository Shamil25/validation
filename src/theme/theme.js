export const theme = {
    pageBackGround: '#E5E5E5',
    cardBackGround: '#fff',
    activeTextColor: '#101D94',
    buttonColor: '#19A527'
}