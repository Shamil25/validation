import styled from "styled-components";

export const Wrapper = styled.div`
    width: 136px;
    height: 20px;
    display: flex;
    justify-content: space-between;
`;
Wrapper.text = styled.span`
    color: ${props => props.theme.activeTextColor};
    padding: 0 10px 0 10px;
`;

