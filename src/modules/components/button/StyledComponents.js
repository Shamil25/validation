import styled from "styled-components";

export const Wrapper = styled.button`
    width: 180px;
    height: 45px;
    background-color: ${props => props.theme.buttonColor};
    color: ${props => props.theme.cardBackGround};
    cursor: pointer;
    padding: 12px 39px 14px 39px;
    border: none;
    border-radius: 5px;
    margin-top: 30px;
`;

Wrapper.text = styled.span`
    font-size: 16px;
    line-height: 19px;
`;