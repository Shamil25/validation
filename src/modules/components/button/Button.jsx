import React from 'react'
import { Wrapper } from './StyledComponents'

const Button = props => {
    return (
        <Wrapper>
            <Wrapper.text>
                Продолжить
            </Wrapper.text>
        </Wrapper>
    )
}

export default Button
