import styled from "styled-components";

export const Wrapper = styled.div`
    width: 300px;
    height: 40px;
    display: flex;
    flex-direction: column;
    margin: 10px;
`;

Wrapper.text = styled.span`
    font-size: 16px;
    line-height: 18px;
    padding-top: 20px;
    color: ${props => props.theme.activeTextColor};
`;
Wrapper.input = styled.input`
    width: ${props => props.placeholder === 'Индекс' && '120px'};
    padding: 0;
    /* width: ${props => props.placeholder === 'Страна' && '120px'}; */
    padding: 10px;
`;  