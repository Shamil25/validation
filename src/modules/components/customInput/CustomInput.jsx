import React from 'react'
import { Wrapper } from './StyledComponents';

const CustomInput = props => {
    const{
        placeholder,
        inputTitle
    } = props
      
    return (
        <Wrapper>
            <Wrapper.text children={inputTitle}/>
            <Wrapper.input placeholder={placeholder}/>
        </Wrapper>
    )
}

export default CustomInput
