import React from 'react';
import {theme} from "../../theme/theme";
import { ThemeProvider } from "styled-components";
import Header from '../../modules/header/Header.jsx';
import Delivery from '../../modules/delivery/Delivery.jsx';
import {
    Wrapper,
} from './StyledComponent';

const MainPage = () => {
    return (
        <ThemeProvider theme={theme}>
            <Wrapper>
                <Wrapper.mainContainer data-at={'wrapper_maincontainer'}>
                    <Header /> 
                    <Delivery /> 
                </Wrapper.mainContainer>
            </Wrapper>
        </ThemeProvider>
    )
}

export default MainPage
