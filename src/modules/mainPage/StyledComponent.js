import styled from "styled-components";

export const Wrapper = styled.div`
    width: 401px;
    height: 520px;
    margin-top: 30px;
    background-color: ${props => props.theme.cardBackGround};
    border-radius: 24px;
`;

Wrapper.mainContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
`;