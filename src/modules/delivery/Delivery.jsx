import React from 'react';
import CustomInput from '../components/customInput/CustomInput.jsx';
import Button from '../components/button/Button.jsx';
import {
    Wrapper,
    Recipient,
} from './StyledComponents';
import Select from 'react-select';

const Delivery = () => {

    const placeHolders = ['ФИО', 'Город', 'Адрес', 'Страна', 'Индекс'];
    const options = [
        { value: 'USA', label: 'USA' },
        { value: 'RUSSIA', label: 'RUSSIA' },
        { value: 'GERMANY', label: 'GERMANY' }
      ]
    return (
        <Wrapper>
            <Wrapper.header>
                Информация для доставки
            </Wrapper.header>
                <Recipient>
                    <CustomInput placeholder={'ФИО'} inputTitle={'Получатель'}/>
                    <CustomInput placeholder={'Город'} inputTitle={'Адрес'}/>
                    <CustomInput placeholder={'Адрес'} />
                    <Recipient.selectWrapper>
                        <Select options={options} defaultInputValue={'Страна'}/>
                        <CustomInput placeholder={'Индекс'}/>
                    </Recipient.selectWrapper>
                    <Button />
                </Recipient>
        </Wrapper>
    );
}

export default Delivery
