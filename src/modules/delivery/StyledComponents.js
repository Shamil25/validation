import styled from "styled-components";

export const Wrapper = styled.div`
    width: 321px;
    height: 384px;
    display: flex;
    flex-direction: column;
    padding-top: 32px;
`;

Wrapper.header = styled.h4`
    width: 321px;
    height: 31px;
    font-size: 26px;
    color: ${props => props.theme.activeTextColor};
    font-family: 'Roboto', sans-serif;
`;

export const Recipient = styled.div`
    width: 320px;
    height: 40px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

Recipient.selectWrapper = styled.div`
    width: 180px;
    height: 100%;
    display: flex;
    margin: 15px 10px;
`;



